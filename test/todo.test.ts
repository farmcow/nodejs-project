import { Request, Response } from "express";
import controller from "../src/controllers/todo";
import { Todo } from "../src/services/todo";

jest.mock("../src/services/todo", () => ({
  Todo: jest.fn(() => ({
    addTodo: jest.fn(),
    getTodoByKey: jest.fn(),
  })),
}));

describe("Todo Controller", () => {
  let req: Partial<Request>;
  let res: Partial<Response>;

  beforeEach(() => {
    req = {
      body: {},
      params: {},
      query: {},
    } as Partial<Request>;

    res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    } as Partial<Response>;
  });

  describe("addTodo", () => {
    it("should add a new todo", async () => {
      req.body = {
        jsonTitle: JSON.stringify({
          key: "myKey",
          value: "value1",
          timestamp: new Date(),
        }),
      };

      await controller.addTodo(req as Request, res as Response);
      expect(res.send).toHaveBeenCalled();
    });

    it("should return 400 if title field is empty", async () => {
      req.body = { jsonTitle: "" };

      await controller.addTodo(req as Request, res as Response);

      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.send).toHaveBeenCalledWith("Title field is mandatory");
    });

    it("should return 500 on error", async () => {
      (Todo as jest.Mock).mockImplementationOnce(() => ({
        addTodo: jest.fn().mockRejectedValue(new Error("Mock error")),
      }));

      await controller.addTodo(req as Request, res as Response);

      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.send).toHaveBeenCalledWith("Mock error");
    });
  });

  describe("getTodoByKey", () => {
    it("should get todo by key", async () => {
      req.params = { key: "testKey" };
      req.query = { timestamp: "testTimestamp" };

      await controller.getTodoByKey(req as Request, res as Response);

      expect(res.send).toHaveBeenCalled();
    });

    it("should return 400 if key is missing", async () => {
      await controller.getTodoByKey(req as Request, res as Response);

      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.send).toHaveBeenCalledWith("Item key is mandatory");
    });

    it("should return 500 on error", async () => {
      (Todo as jest.Mock).mockImplementationOnce(() => ({
        getTodoByKey: jest.fn().mockRejectedValue(new Error("Mock error")),
      }));

      req.params = { key: "testKey" };

      await controller.getTodoByKey(req as Request, res as Response);

      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.send).toHaveBeenCalledWith("Mock error");
    });
  });
});
