const express = require('express');
import { config } from 'dotenv';
import { connectToDatabase } from './db';
import { resolve } from 'path';
import helmet from 'helmet';
import rateLimit from 'express-rate-limit';
import { notFoundHandler } from './src/middleware'
import routes from './src/routes';

config({ path: resolve(__dirname, './.env') });

const port = process.env.PORT;
const app = express();

const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100, // limit each IP to 100 requests per windowMs
    message: 'Too many requests' // message to send
});

app.use(helmet());
app.use(limiter);

// Use the express.json() middleware with a payload size limit of 10kb
app.use(express.json({ limit: '10kb' }));

app.use('/api/v1/todo', routes.todo);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});

// Define custom error handling middleware
app.use(notFoundHandler);

// Initialize the database connection pool at the start of your application
connectToDatabase();

export default app;