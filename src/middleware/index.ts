import { Request, Response } from 'express';
import { Status } from '../constants/Status'

export const notFoundHandler = (req: Request, res: Response) => {
    res.status(Status.code404).send(Status.code404Message);
};