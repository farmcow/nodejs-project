import { getPool } from "../../db";
import { PoolConnection } from "mysql2/promise";
import { ResultSetHeader, RowDataPacket } from "mysql2";
import { ITodo } from "../models/todo";
import { Status } from "../constants/Status";

export class Todo {
  pool = getPool();

  async addTodo(todo: ITodo): Promise<any> {
    let connection: PoolConnection | null = null;
    try {
      // Get a connection from the pool
      connection = await this.pool.getConnection();

      // convert 12 hour format to timestamp
      const timestamp = this.convertTimeStringToTimestamp();

      // reassign json object
      const data = {
        key: todo.key,
        value: todo.value,
        timestamp,
      };

      // Check if the key already exists in the database
      const [existingRows] = await connection.execute<RowDataPacket[]>(
        "SELECT * FROM todos WHERE referralKey = ?",
        [todo.key]
      );

      if (existingRows.length > 0) {
        // Get the existing todo object
        const existingTodo = existingRows[0];

        // Parse the jsonTitle string to an object
        const jsonData = JSON.parse(existingTodo.jsonTitle);

        // Update the value & timestamp property
        jsonData.value = todo.value;
        jsonData.timestamp = timestamp;

        // Stringify the updated object
        const updatedJson = JSON.stringify(jsonData);

        // If the key exists, update the existing record
        await connection.execute(
          "UPDATE todos SET jsonTitle = ? WHERE referralKey = ?",
          [updatedJson, todo.key]
        );

        return {
          status: Status.code200,
          message: "Todo successfully updated.",
          data: { ...todo },
        };
      } else {
        // Insert the todo into the database
        const [result] = await connection.execute<ResultSetHeader>(
          "INSERT INTO todos (jsonTitle, referralKey) VALUES (?, ?)",
          [JSON.stringify(data), todo.key]
        );

        // Get the ID of the inserted row
        const insertId = result.insertId;

        // Return the inserted Todo object with the generated ID
        return {
          status: Status.code200,
          message: "Todo successfully added.",
          data: { ...todo, id: insertId },
        };
      }
    } catch (error) {
      console.error("Error adding todo:", error);
      // Rethrow the error to be handled by the caller
      throw error;
    } finally {
      // Release the connection back to the pool
      if (connection) {
        connection.release();
      }
    }
  }

  convertTimeStringToTimestamp() {
    const currentDate = new Date();

    // Convert the current date and time to a timestamp
    const timestamp = Math.floor(currentDate.getTime() / 1000);

    return timestamp;
  }

  async getTodoByKey(key: string, timestamp?: string): Promise<any | null> {
    let connection: PoolConnection | null = null;
    try {
      // Get a connection from the pool
      connection = await this.pool.getConnection();

      let query: string;
      const params = [key];

      if (timestamp) {
        query =
          "SELECT * FROM todos WHERE referralKey = ? AND JSON_EXTRACT(jsonTitle, '$.timestamp') < ?";
        params.push(timestamp);
      } else {
        query = "SELECT * FROM todos WHERE referralKey = ?";
      }

      const [row] = await connection.execute<RowDataPacket[]>(query, params);

      // If no todo found, return null
      if (!row || row.length === 0) {
        return {
          status: Status.code400,
          message: "Item not found.",
        };
      }

      // Extract the first row (should be the only one)
      const todoRow = row[0];

      // Parse the jsonTitle string to an object
      const jsonData = JSON.parse(todoRow.jsonTitle);

      // Create and return the ITodo object
      return {
        value: jsonData.value,
      };
    } catch (error) {
      console.error("Error fetching todos:", error);
      // Rethrow the error to be handled by the caller
      throw error;
    } finally {
      // Release the connection back to the pool
      if (connection) {
        connection.release();
      }
    }
  }
}
