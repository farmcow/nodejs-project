import { Todo } from "../services/todo";
import { Request, Response } from 'express';
import { Status } from '../constants/Status'

// add todo
async function addTodo(req: Request, res: Response) {
    try {
        const { jsonTitle } = req.body;

        // validation
        if (jsonTitle == "")
            return res.status(Status.code400).send("Title field is mandatory");
        
        // insert todo list  
        const todoObj = new Todo();
        const newTodo = await todoObj.addTodo(req.body);
        res.send(newTodo);
    } catch (e: any) {
      console.log('Error ' + e);
      res.status(Status.code500).send(e.message);
    }
}

async function getTodoByKey(req: Request, res: Response) {
  try {
    const { key } = req.params;
    let { timestamp } = req.query;
    
    if (!key) res.status(400).send("Item key is mandatory");

    // Ensure timestamp is a string or undefined
    if (typeof timestamp !== 'string') {
        timestamp = undefined;
      }

    // get todo item
    const todoObj = new Todo();
    const todoItem = await todoObj.getTodoByKey(key, timestamp as string);
    res.send(todoItem);
  } catch (e: any) {
    console.log('Error ' + e);
    res.status(Status.code500).send(e.message);
  }
}

export default {
    addTodo,
    getTodoByKey
};