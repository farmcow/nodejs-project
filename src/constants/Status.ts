export class Status {

    public static code400 = 400
    public static code400Message = "Bad Request"

    public static code404 = 404
    public static code404Message = "Method Not Found" 

    public static code200 = 200

    public static code500 = 500
}