import { Router } from "express";
import TodoController from "../controllers/todo";

const router = Router();

// Create
router.post("/", TodoController.addTodo);

// Get by id
router.get("/:key", TodoController.getTodoByKey);

export default router;