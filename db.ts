import { createPool, Pool } from 'mysql2/promise';

let pool: Pool;

export const connectToDatabase = () => {
  const port = process.env.DB_PORT ? parseInt(process.env.DB_PORT) : 3306;

  pool = createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port
  });
};

export const getPool = () => {
  if (!pool) {
    throw new Error('Database pool has not been initialized');
  }
  return pool;
};
